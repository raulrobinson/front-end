import { Inject, InjectionToken, NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { HomeComponent } from './home/home.component';
import { MsalGuard } from '@azure/msal-angular';

const routes: Routes = [
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [
      MsalGuard
    ]
  },
  {
    // Needed for hash routing
    path: 'code',
    component: HomeComponent
  },
  {
    path: '',
    component: HomeComponent
  },
  {
    // Needed for Error routing
    path: 'error',
    component: HomeComponent
  }
];

const isIframe = window !== window.parent && !window.opener;

export const routerOptions: ExtraOptions = {
  initialNavigation: 'enabledNonBlocking', //'disabled' | 'enabled' | 'enabledBlocking' | 'enabledNonBlocking'
  useHash: true,
}

@NgModule({
  imports: [RouterModule.forRoot(routes, routerOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
