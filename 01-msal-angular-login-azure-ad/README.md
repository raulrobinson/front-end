# MsalAngularLogin

<img src="images/diagram-01-auth-code-flow.png">

Inicio de sesión de usuarios y llamada a Microsoft Graph API desde una aplicación de página única (SPA) de Angular mediante el uso del flujo de código de autenticación.

La aplicación de ejemplo que se crea con este tutorial permite que una aplicación de página única de Angular haga consultas a Microsoft Graph API o a una API web que acepte los tokens que emite la Plataforma de identidad de Microsoft. Usa la Biblioteca de autenticación de Microsoft (MSAL) para Angular v2, un contenedor de la biblioteca principal de MSAL.js v2. MSAL Angular permite que las aplicaciones de Angular 9 y versiones posteriores autentiquen a usuarios empresariales mediante Azure Active Directory, a usuarios de cuentas de Microsoft y a usuarios de identidades de redes sociales como Facebook, Google y LinkedIn. La biblioteca también permite que las aplicaciones obtengan acceso a los servicios en la nube de Microsoft y a Microsoft Graph.

En este escenario, después de que un usuario inicia sesión, se solicita un token de acceso y se agrega a las solicitudes HTTP mediante el encabezado de autorización. MSAL controla la adquisición y la renovación de tokens.

Bibliotecas
En este tutorial se usan las siguientes bibliotecas:

- MSAL Angular:	Biblioteca de autenticación de Microsoft para el contenedor de Angular de JavaScript
- Explorador MSAL:	Biblioteca de autenticación de Microsoft para el paquete del explorador de JavaScript v2

Puede encontrar el código fuente de todas las bibliotecas MSAL.js en el repositorio AzureAD/microsoft-authentication-library-for-js de GitHub.

<img src="images/PackageStructure.png">

# Creación del proyecto

Una vez que tenga Node.js instalado, abra una ventana de terminal y ejecute los siguientes comandos para generar una nueva aplicación de Angular:

Install the Angular CLI
- npm install -g @angular/cli   

Generate a new Angular app
- ng new msal-angular-tutorial --routing=true --style=css --strict=false

Change to the app directory
- cd msal-angular-tutorial                           

Install the Angular Material component library (optional, for UI)
- npm install @angular/material @angular/cdk         

Install MSAL Browser and MSAL Angular in your application
- npm install @azure/msal-browser @azure/msal-angular

To add a home page
- ng generate component home                          

To add a profile page
- ng generate component profile

# Registrar su aplicación

Siga las instrucciones para registrar una aplicación de página única en Azure Portal.
En la página de información general del registro de la aplicación, anote el valor de Id. de aplicación (cliente) para su uso posterior.
Registre el valor de URI de redirección como http://localhost:4200/ y escriba como "SPA". 

# Configuración de la aplicación

En la carpeta src/app, edite app.module.ts 

- Enter_the_Application_Id_Here:	En la página de información general del registro de la aplicación, corresponde al valor de Id. de aplicación (cliente) .
- Enter_the_Cloud_Instance_Id_Here:	Esta es la instancia de la nube de Azure. En el caso de la nube principal o global de Azure, escriba https://login.microsoftonline.com. Para las nubes nacionales (por ejemplo, China) consulte Nubes nacionales.
- Enter_the_Tenant_Info_Here:	Seleccione una de las siguientes opciones: Si la aplicación admite las cuentas de este directorio organizativo, reemplace este valor por el identificador del directorio (inquilino) o por el nombre del inquilino (por ejemplo, contoso.microsoft.com). Si la aplicación admite cuentas en cualquier directorio organizativo, reemplace este valor por organizaciones. Si la aplicación admite cuentas en cualquier directorio organizativo y cuentas Microsoft personales, reemplace este valor por común. Para restringir la compatibilidad a Personal Microsoft accounts only (Solo cuentas Microsoft personales), reemplace este valor por consumidores.
- Enter_the_Redirect_Uri_Here:	Reemplácelo por http://localhost:4200 .

# Prueba del código

Inicie el servidor web para que escuche el puerto; para ello, ejecute los siguientes comandos desde la carpeta de la aplicación en un símbolo del sistema de la línea de comandos:

- npm install
- ng serve

<img src="images/angular-01-not-signed-in.png">

Consentimiento para el acceso a la aplicación
La primera vez que inicie sesión en la aplicación, se le pedirá que le conceda acceso a su perfil y que le permita iniciar sesión:

<img src="images/spa-02-consent-dialog.png">

Si da su consentimiento a los permisos solicitados, la aplicación web muestra una página que indica que el inicio de sesión se ha realizado correctamente:

<img src="images/angular-02-signed-in.png">

Después de iniciar sesión, seleccione Perfil para ver la información de perfil de usuario devuelta en la respuesta de la llamada a Microsoft Graph API:

<img src="images/angular-03-profile-data.png">


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Angular version.
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.8.
